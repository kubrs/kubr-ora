-- Create sequence 
create sequence SEQ_TREGISTRATIONS
minvalue 100
maxvalue 9999999999999999999999999999
start with 100
increment by 20
cache 20;
