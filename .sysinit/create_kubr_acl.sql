
GRANT EXECUTE ON UTL_HTTP to KUBR;
GRANT EXECUTE ON DBMS_CQ_NOTIFICATION TO KUBR; 
GRANT CHANGE NOTIFICATION TO KUBR;
ALTER SYSTEM SET "JOB_QUEUE_PROCESSES"=4;

DECLARE
  acl_name       varchar2(50) := 'acl-kubr-dev';
  acl_host       varchar2(50) := 'localhost';
  acl_lower_port number := 8080;
  acl_upper_port number := 8088;
  acl_path       VARCHAR2(4000);
  acl_schema     varchar2(30) := 'KUBR';
BEGIN
  SELECT acl
    INTO acl_path
    FROM dba_network_acls
   WHERE host = acl_host
  --AND lower_port IS NULL
  --AND upper_port IS NULL
  ;

  IF dbms_network_acl_admin.check_privilege(acl_path, acl_schema, 'connect') IS NULL THEN
    dbms_network_acl_admin.add_privilege(acl_path,
                                         acl_schema,
                                         TRUE,
                                         'connect');
    COMMIT;
  END IF;
  
EXCEPTION

  WHEN NO_DATA_FOUND THEN
    DBMS_NETWORK_ACL_ADMIN.create_acl(acl         => acl_name,
                                      description => 'Allow ' || acl_schema ||
                                                     ' access to ' ||
                                                     acl_host,
                                      principal   => acl_schema,
                                      is_grant    => TRUE,
                                      privilege   => 'connect',
                                      start_date  => SYSTIMESTAMP,
                                      end_date    => NULL);
  
    DBMS_NETWORK_ACL_ADMIN.assign_acl(acl        => acl_name,
                                      host       => acl_host,
                                      lower_port => acl_lower_port,
                                      upper_port => acl_upper_port);
  
    COMMIT;

END;


