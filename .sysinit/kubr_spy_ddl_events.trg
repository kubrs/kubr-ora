CREATE OR REPLACE NONEDITIONABLE TRIGGER kubr_spy_ddl_events
    BEFORE DDL ON DATABASE
DECLARE
    vDDLEvent kubr.tkeyddlatempts%ROWTYPE;
    CURSOR cSysEvent IS
        SELECT sys_context('USERENV', 'OS_USER') AS sc_os_user
              ,sys_context('USERENV', 'CLIENT_PROGRAM_NAME') AS sc_client_program_name
              ,systimestamp AS ora_timestamp
              ,ora_database_name AS ora_database_name
              ,ora_login_user AS ora_login_user
              ,ora_sysevent AS ora_sysevent
              ,ora_dict_obj_type AS ora_dict_obj_type
              ,ora_dict_obj_owner AS ora_dict_obj_owner
              ,ora_dict_obj_name AS ora_dict_obj_name
          FROM dual;
BEGIN
    -- Record event
    FOR rec IN cSysEvent LOOP
        vDDLEvent.sc_os_user             := rec.sc_os_user;
        vDDLEvent.sc_client_program_name := rec.sc_client_program_name;
        vDDLEvent.ora_systime            := rec.ora_timestamp;
        vDDLEvent.ora_database_name      := rec.ora_database_name;
        vDDLEvent.ora_login_user         := rec.ora_login_user;
        vDDLEvent.ora_sysevent           := rec.ora_sysevent;
        vDDLEvent.ora_dict_obj_type      := rec.ora_dict_obj_type;
        vDDLEvent.ora_dict_obj_owner     := rec.ora_dict_obj_owner;
        vDDLEvent.ora_dict_obj_name      := rec.ora_dict_obj_name;
    END LOOP;
    -- Validate the event
    kubr.pkgClaw.auditDDLEvent(vDDLEvent);
END;
/
