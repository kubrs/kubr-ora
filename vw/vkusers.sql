CREATE OR REPLACE VIEW vkusers AS 
SELECT t.id, jt.*
  FROM tkusers t,
       json_table(BODY, '$'
         COLUMNS (USERNAME VARCHAR2(10) PATH '$.USERNAME',
                  NICKNAME  VARCHAR2(20) PATH '$.NICKNAME',
                  MOTTO  VARCHAR2(20) PATH '$.MOTTO',
                  THEME  VARCHAR2(20) PATH '$.THEME',
                  ROLE  VARCHAR2(20) PATH '$.ROLE',
                  AVATAR  VARCHAR2(20) PATH '$.AVATAR',
                  VILLAGE VARCHAR2(20) PATH '$.VILLAGE',
                  AUTHOR  VARCHAR2(20) PATH '$.AUTHOR')) AS "JT";
                
