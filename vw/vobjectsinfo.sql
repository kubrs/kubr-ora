CREATE OR REPLACE VIEW VOBJECTSINFO AS
WITH contribs AS
 (SELECT h.obj_type || '.' || c.objectqualifiedname AS objfullname
        ,h.obj_type
        ,h.obj_owner
        ,h.obj_name
        ,h.obj_status
        ,h.usr_who
        ,h.usr_why
        ,c.command
        ,c.changedwhen
        ,c.changedwho
        ,c.changedwhy
    FROM thousekeeper h
   INNER JOIN tcontributors c
      ON c.objecttype = h.obj_type
     AND c.objectqualifiedname = h.obj_owner || '.' || h.obj_name),
regs AS
 (SELECT r.ora_dict_obj_type || '.' || r.ora_dict_obj_owner || '.' || r.ora_dict_obj_name AS objfullname
        ,r.ora_dict_obj_type
        ,r.ora_dict_obj_owner
        ,r.ora_dict_obj_name
        ,r.ora_login_user
        ,r.ora_systime
        ,r.sc_os_user
        ,r.sc_client_program_name
        ,t.listening_on
    FROM tregistrations r
   INNER JOIN tregistration_targets t
      ON t.reg_id = r.id)

SELECT cs.changedwhen AS command_timestamp
      ,cs.command     AS command
      ,cs.usr_who     AS author
      ,cs.usr_why     AS motive
      ,cs.obj_type
      ,cs.obj_owner
      ,cs.obj_name
      ,cs.obj_status
       --,cs.changedwho 
       --,cs.changedwhy
      ,rs.ora_login_user         AS reg_user
      ,rs.ora_systime            AS reg_timestamp
      ,rs.listening_on           AS reg_event
      ,rs.sc_os_user             AS reg_os_user
      ,rs.sc_client_program_name AS reg_client_application
  FROM contribs cs
  LEFT OUTER JOIN regs rs
    ON cs.objfullname = rs.objfullname;
