CREATE OR REPLACE NONEDITIONABLE PACKAGE pkgClaw IS

    -- Author  : CEZAR@SHUTTLELITE
    -- Purpose : The brain of all Claw processes. 

    -- Public function and procedure declarations
    PROCEDURE auditDDLEvent(pDDLEvent IN tkeyddlatempts%ROWTYPE);
    FUNCTION isKeyDDLEvent(pDDLEvent IN tkeyddlatempts%ROWTYPE) RETURN BOOLEAN;
    FUNCTION isDbObjectLocked
    (
        pDbObject     IN typeDbObject
       ,pHousekeeping IN OUT thousekeeper%ROWTYPE
    ) RETURN BOOLEAN;
    FUNCTION isDbObjectLocked
    (
        pdbObjOwner   IN VARCHAR2
       ,pdbObjType    IN VARCHAR2
       ,pdbObjName    IN VARCHAR2
       ,pHousekeeping IN OUT thousekeeper%ROWTYPE
    ) RETURN BOOLEAN;
    FUNCTION lockDbObject
    (
        pRepoObject IN OUT typeRepoObject
       ,pDbObject   IN OUT typeDbObject
    ) RETURN BOOLEAN;
    FUNCTION lockDbObject
    (
        pRepoPath   IN VARCHAR2
       ,pFilePath   IN VARCHAR2
       ,pRepoUser   IN VARCHAR2
       ,pRepoComm   IN VARCHAR2
       ,pSteal      IN VARCHAR2
       ,pdbObjOwner IN VARCHAR2
       ,pdbObjType  IN VARCHAR2
       ,pdbObjName  IN VARCHAR2
    ) RETURN BOOLEAN;
    FUNCTION releaseDbObject
    (
        pRepoObject IN OUT typeRepoObject
       ,pDbObject   IN OUT typeDbObject
    ) RETURN BOOLEAN;
    FUNCTION releaseDbObject
    (
        pRepoPath   IN VARCHAR2
       ,pFilePath   IN VARCHAR2
       ,pRepoUser   IN VARCHAR2
       ,pdbObjOwner IN VARCHAR2
       ,pdbObjType  IN VARCHAR2
       ,pdbObjName  IN VARCHAR2
    ) RETURN BOOLEAN;
    FUNCTION existsDbObject(pDbObject IN typeDbObject) RETURN BOOLEAN;

END pkgClaw;
/
CREATE OR REPLACE NONEDITIONABLE PACKAGE BODY pkgClaw IS

    PROCEDURE initialize IS
    BEGIN
        NULL;
    END initialize;

    FUNCTION setSysEvent
    (
        pSysEvent IN VARCHAR2
       ,pDbObject IN typedbobject
    ) RETURN VARCHAR2 IS
        vres VARCHAR2(10) := pSysEvent;
    BEGIN
        IF existsDbObject(pDbObject) THEN
            IF (pSysEvent = Pkgconstant.DDLSystemEvent.sysCREATE) THEN
                vres := Pkgconstant.DDLSystemEvent.sysREPLACE;
            END IF;
        END IF;
        RETURN vres;
    END setSysEvent;

    -- Log DDL Event
    PROCEDURE logDDLEvent
    (
        pDDLEvent     IN tkeyddlatempts%ROWTYPE
       ,pHousekeeping IN thousekeeper%ROWTYPE
       ,pLocked       IN BOOLEAN
    ) IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        vLockedSignal VARCHAR2(10);
        vDbObject     typedbobject;
        vSysEvent     tkeyddlatempts.ora_sysevent%TYPE;
        vDescription  tkeyddlatempts.description%TYPE;
    BEGIN
        vDbObject         := typedbobject(pDDLEvent.ora_dict_obj_owner
                                         ,pDDLEvent.ora_dict_obj_type
                                         ,pDDLEvent.ora_dict_obj_name);
        vDbObject.userWho := USER;
        vLockedSignal := CASE
                             WHEN NOT pLocked THEN
                              pkgConstant.signal.passed
                             ELSE
                              pkgConstant.signal.failed
                         END;
        vSysEvent         := setSysEvent(pDDLEvent.ora_sysevent, vDbObject);
        vDescription      := pkgMisc.toString(pHousekeeping);
        -- Audit event
        INSERT INTO tkeyddlatempts
            (ora_systime
            ,ora_database_name
            ,ora_login_user
            ,ora_sysevent
            ,ora_dict_obj_type
            ,ora_dict_obj_owner
            ,ora_dict_obj_name
            ,sc_os_user
            ,sc_client_program_name
            ,status
            ,description)
        VALUES
            (pDDLEvent.ora_systime
            ,pDDLEvent.ora_database_name
            ,pDDLEvent.ora_login_user
            ,vSysEvent
            ,pDDLEvent.ora_dict_obj_type
            ,pDDLEvent.ora_dict_obj_owner
            ,pDDLEvent.ora_dict_obj_name
            ,pDDLEvent.sc_os_user
            ,pDDLEvent.sc_client_program_name
            ,vLockedSignal
            ,vDescription);
        COMMIT;
    END logDDLEvent;

    -- Audit DDL Event
    PROCEDURE auditDDLEvent(pDDLEvent IN tkeyddlatempts%ROWTYPE) IS
        vLocked       BOOLEAN := FALSE;
        vDbObject     typedbobject;
        vHousekeeping thousekeeper%ROWTYPE;
    BEGIN
        vDbObject         := typedbobject(pDDLEvent.ora_dict_obj_owner
                                         ,pDDLEvent.ora_dict_obj_type
                                         ,pDDLEvent.ora_dict_obj_name);
        vDbObject.userWho := pDDLEvent.ora_login_user;
    
        IF (pDDLEvent.Ora_Dict_Obj_Owner = pkgConstant.application.self) THEN
            pkgException.raiseException(pkgException.exceptionList.claw_almighty_code, vDbObject);
        END IF;
    
        IF isKeyDDLEvent(pDDLEvent) THEN
            -- Cancel event when target object is locked
            vLocked := kubr.pkgClaw.isDbObjectLocked(vDbObject, vHousekeeping);
            logDDLEvent(pDDLEvent, vHousekeeping, vLocked);
        
            IF vLocked THEN
                pkgException.raiseException(pkgException.exceptionList.claw_obj_is_locked_code, vDbObject);
            END IF;
        END IF;
    END auditDDLEvent;

    -- Validate current DDL Event - is this a key observable event?
    FUNCTION isKeyDDLEvent(pDDLEvent IN tkeyddlatempts%ROWTYPE) RETURN BOOLEAN IS
        vres BOOLEAN := FALSE;
    BEGIN
        IF (pDDLEvent.ora_sysevent IN ('ALTER', 'DROP', 'TRUNCATE', 'CREATE')) THEN
            vres := TRUE;
        END IF;
        RETURN vres;
    END isKeyDDLEvent;

    -- Check if the given DB Object has a LOCKed flag associated
    FUNCTION isDbObjectLocked
    (
        pDbObject     IN typeDbObject
       ,pHousekeeping IN OUT thousekeeper%ROWTYPE
    ) RETURN BOOLEAN IS
        vres BOOLEAN := FALSE;
        CURSOR cLockedObject IS
            SELECT t.*
              FROM thousekeeper t
             WHERE t.obj_owner = pDbObject.dbObjOwner
               AND t.obj_type = pDbObject.dbObjType
               AND t.obj_name = pDbObject.dbObjName;
    BEGIN
        FOR rec IN cLockedObject LOOP
            IF (rec.obj_status = pkgConstant.application.dbObjectLocked) THEN
                pHousekeeping := rec;
                vres          := TRUE;
            END IF;
        END LOOP;
        RETURN vres;
    END isDbObjectLocked;

    -- Check if the given DB Object has a LOCKed flag associated
    FUNCTION isDbObjectLocked
    (
        pdbObjOwner   IN VARCHAR2
       ,pdbObjType    IN VARCHAR2
       ,pdbObjName    IN VARCHAR2
       ,pHousekeeping IN OUT thousekeeper%ROWTYPE
    ) RETURN BOOLEAN IS
        pDbObject typedbobject;
    BEGIN
        pDbObject         := typedbobject(pdbObjOwner, pdbObjType, pdbObjName);
        pDbObject.userWho := USER;
        RETURN isDbObjectLocked(pDbObject, pHousekeeping);
    END isDbObjectLocked;

    -- Keep track of SVN contributors
    PROCEDURE createContribution
    (
        pRepoObject IN typeRepoObject
       ,pDbObject   IN typeDbObject
    ) IS
    BEGIN
        INSERT INTO tcontributors
            (objecttype, objectqualifiedname, command, changedwhen, changedwho, obj_repo)
        VALUES
            (pDbObject.getType
            ,pDbObject.getQualifiedName
            ,pDbObject.getCommand
            ,systimestamp
            ,pDbObject.userWho
            ,pRepoObject);
    END createContribution;

    -- Perform internal objects housekeeping (lock or release database objects)
    PROCEDURE houseKeeping
    (
        pRepoObject IN typeRepoObject
       ,pDbObject   IN typeDbObject
    ) IS
    BEGIN
        -- delete (release) or insert (lock) on housekeeper table
        IF (pDbObject.getCommand = pkgVCS.getCommandCode(pkgVCS.setLock)) THEN
            INSERT INTO thousekeeper
                (obj_owner, obj_type, obj_name, obj_status, usr_who, obj_repo)
            VALUES
                (pDbObject.getOwner
                ,pDbObject.getType
                ,pDbObject.getName
                ,pkgConstant.application.dbObjectLocked
                ,pDbObject.userWho
                ,pRepoObject);
        ELSIF (pDbObject.getCommand = pkgVCS.getCommandCode(pkgVCS.releaseLock)) THEN
            DELETE FROM thousekeeper h
             WHERE h.obj_owner = pDbObject.getOwner
               AND h.obj_name = pDbObject.getName
               AND h.obj_type = pDbObject.getType
               AND h.usr_who = pDbObject.userWho
               AND h.obj_repo.repoUser = pRepoObject.repoUser
               AND h.obj_repo.repoPath = pRepoObject.repoPath
               AND h.obj_repo.filePath = pRepoObject.filePath;
        
            IF (SQL%ROWCOUNT = 0) THEN
                RAISE pkgException.claw_abusive_release_try;
            END IF;
        END IF;
    END houseKeeping;

    -- Set new state to specific database object
    PROCEDURE changeDbObjectState
    (
        pRepoObject IN typeRepoObject
       ,pDbObject   IN typeDbObject
    ) IS
    BEGIN
        createContribution(pRepoObject, pDbObject);
        housekeeping(pRepoObject, pDbObject);
    EXCEPTION
        WHEN pkgException.unique_contraint_violated THEN
            pkgException.raiseException(pkgException.exceptionList.claw_obj_already_locked_code, pDbObject);
        WHEN pkgException.claw_abusive_release_try THEN
            pkgException.raiseException(pkgException.exceptionList.claw_abusive_release_try_code);
        WHEN OTHERS THEN
            pkgException.raiseException(SQLCODE, SQLERRM);
    END changeDbObjectState;

    -- Check the existance of a given object in the database at the moment of the <Set Lock> SVN Event
    FUNCTION existsDbObject(pDbObject IN typeDbObject) RETURN BOOLEAN IS
        CURSOR cvalidobj IS
            SELECT 1
              FROM dba_objects ao
             WHERE 1 = 1
               AND ao.owner = pDbObject.getowner()
               AND ao.object_type = pDbObject.gettype()
               AND ao.object_name = pDbObject.getname();
    BEGIN
        FOR rec IN cvalidobj LOOP
            RETURN(TRUE);
        END LOOP;
        RETURN(FALSE);
    END existsDbObject;

    -- Set Lock on database object based on given typeDbObject
    FUNCTION processDbObject
    (
        pRepoObject IN OUT typeRepoObject
       ,pDbObject   IN OUT typeDbObject
       ,pCommand    IN pkgVCS.Command
    ) RETURN BOOLEAN IS
    BEGIN
        pkgvcs.kubrResponse.code    := NULL;
        pkgvcs.kubrResponse.message := NULL;
    
        SAVEPOINT behindTheRedLine;
        IF (existsDbObject(pDbObject)) THEN
            pDbObject.setCommand(pkgVCS.getCommandCode(pCommand));
            changeDbObjectState(pRepoObject => pRepoObject, pDbObject => pDbObject);
        ELSE
            pkgVCS.KubrResponse.code    := pkgException.exceptionList.undefined_dbobject_code;
            pkgVCS.KubrResponse.message := pDbObject.getFullyQualifiedName();
            pkgException.raiseException(pkgException.exceptionList.undefined_dbobject_code
                                       ,pDbObject.getFullyQualifiedName());
        END IF;
        COMMIT;
        RETURN(TRUE);
    EXCEPTION
        WHEN OTHERS THEN
            pkgVCS.KubrResponse.code    := nvl(SQLCODE, 0);
            pkgVCS.KubrResponse.message := nvl(SQLERRM, Pkgconstant.signs.mi);
            ROLLBACK TO SAVEPOINT behindTheRedLine;
            RETURN(FALSE);
    END processDbObject;

    -- Set Lock on database object based on given typeDbObject
    FUNCTION lockDbObject
    (
        pRepoObject IN OUT typeRepoObject
       ,pDbObject   IN OUT typeDbObject
    ) RETURN BOOLEAN IS
    BEGIN
        pDbObject.userWho := USER;
        RETURN(processDbObject(pRepoObject, pDbObject, pkgVCS.setLock));
    END lockDbObject;

    -- Set Lock on database object based on given typeDbObject
    FUNCTION lockDbObject
    (
        pRepoPath   IN VARCHAR2
       ,pFilePath   IN VARCHAR2
       ,pRepoUser   IN VARCHAR2
       ,pRepoComm   IN VARCHAR2
       ,pSteal      IN VARCHAR2
       ,pdbObjOwner IN VARCHAR2
       ,pdbObjType  IN VARCHAR2
       ,pdbObjName  IN VARCHAR2
    ) RETURN BOOLEAN IS
        pDbObject   typedbobject;
        pRepoObject typeRepoObject;
        vres        BOOLEAN;
    BEGIN
        SAVEPOINT iThinkISawAnUFO;
        pRepoObject       := typeRepoObject(pRepoPath, pFilePath, pRepoUser, pRepoComm, pSteal);
        pDbObject         := typedbobject(pdbObjOwner, pdbObjType, pdbObjName);
        pDbObject.userWho := USER;
        vres              := processDbObject(pRepoObject, pDbObject, pkgVCS.setLock);
        IF (vres) THEN
            COMMIT;
        ELSE
            ROLLBACK TO iThinkISawAnUFO;
        END IF;
        RETURN(vres);
    END lockDbObject;

    -- Release Lock on database object based on given TypeDbObject
    FUNCTION releaseDbObject
    (
        pRepoObject IN OUT typeRepoObject
       ,pDbObject   IN OUT typeDbObject
    ) RETURN BOOLEAN IS
    BEGIN
        pDbObject.userWho := USER;
        RETURN(processDbObject(pRepoObject, pDbObject, pkgVCS.releaseLock));
    END releaseDbObject;

    -- Release Lock on database object based on given TypeDbObject
    FUNCTION releaseDbObject
    (
        pRepoPath   IN VARCHAR2
       ,pFilePath   IN VARCHAR2
       ,pRepoUser   IN VARCHAR2
       ,pdbObjOwner IN VARCHAR2
       ,pdbObjType  IN VARCHAR2
       ,pdbObjName  IN VARCHAR2
    ) RETURN BOOLEAN IS
        pRepoObject typeRepoObject;
        pDbObject   typedbobject;
        vres        BOOLEAN;
    BEGIN
        SAVEPOINT iThinkISawAnUFO;
        pRepoObject       := typeRepoObject(pRepoPath, pFilePath, pRepoUser, NULL, NULL);
        pDbObject         := typedbobject(pdbObjOwner, pdbObjType, pdbObjName);
        pDbObject.userWho := USER;
        vres              := processDbObject(pRepoObject, pDbObject, pkgVCS.releaseLock);
        IF (vres) THEN
            COMMIT;
        ELSE
            ROLLBACK TO iThinkISawAnUFO;
        END IF;
        RETURN(vres);
    END releaseDbObject;

    PROCEDURE cleanup IS
    BEGIN
        NULL;
    END cleanup;

BEGIN
    -- Initialization
    initialize;
END pkgClaw;
/
