CREATE OR REPLACE PACKAGE pkgQueryNotifier IS

    -- Author  : CEZAR@SHUTTLELITE
    -- Created : 7/16/2017
    -- Purpose : Continuous Query Notifications Manager

    PROCEDURE prcRegisterQueries;
    PROCEDURE prcDeregisterQueries;
    PROCEDURE prcDeregisterQuery(pRegid IN user_cq_notification_queries.regid%TYPE);

    PROCEDURE prcQNCallback(ntfnds IN CQ_NOTIFICATION$_DESCRIPTOR);

END pkgQueryNotifier;
/
CREATE OR REPLACE PACKAGE BODY pkgQueryNotifier IS

    PROCEDURE prcRegisterQueries IS
        reginfo  CQ_NOTIFICATION$_REG_INFO;
        v_cursor SYS_REFCURSOR;
        regid    NUMBER;
    
    BEGIN
        /* 1. Construct registration information.
        chnf_callback is name of notification handler.
        QOS_QUERY specifies result-set-change notifications. */
        reginfo := cq_notification$_reg_info('pkgQueryNotifier.prcQNCallback', DBMS_CQ_NOTIFICATION.QOS_QUERY, 0, 0, 0);
    
        /* 2. Create registration. */
        regid := DBMS_CQ_NOTIFICATION.new_reg_start(reginfo);
        OPEN v_cursor FOR
            SELECT dbms_cq_notification.CQ_NOTIFICATION_QUERYID
                  ,t.obj_owner
                  ,t.obj_type
                  ,t.obj_name
                  ,t.obj_status
              FROM kubr.thousekeeper t
             WHERE t.obj_type = 'TABLE';
        CLOSE v_cursor;
    
        OPEN v_cursor FOR
            SELECT dbms_cq_notification.CQ_NOTIFICATION_QUERYID
                  ,t.obj_owner
                  ,t.obj_type
                  ,t.obj_name
                  ,t.obj_status
              FROM kubr.thousekeeper t
             WHERE t.obj_type = 'VIEW';
        CLOSE v_cursor;
    
        --DBMS_CQ_NOTIFICATION.reg_end;
    END prcRegisterQueries;

    PROCEDURE prcDeregisterQueries IS
    BEGIN
        FOR rec IN (SELECT regid FROM user_cq_notification_queries) LOOP
            DBMS_CQ_NOTIFICATION.DEREGISTER(rec.regid);
        END LOOP;
    END prcDeregisterQueries;

    PROCEDURE prcDeregisterQuery(pRegid IN user_cq_notification_queries.regid%TYPE) IS
    BEGIN
        DBMS_CQ_NOTIFICATION.DEREGISTER(pRegid);
    END prcDeregisterQuery;

    PROCEDURE prcQNCallback(ntfnds IN CQ_NOTIFICATION$_DESCRIPTOR) IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        regid          NUMBER;
        tbname         VARCHAR2(60);
        event_type     NUMBER;
        numtables      NUMBER;
        operation_type NUMBER;
        numrows        NUMBER;
        row_id         VARCHAR2(2000);
        numqueries     NUMBER;
        qid            NUMBER;
        qop            NUMBER;
    
    BEGIN
        regid      := ntfnds.registration_id;
        event_type := ntfnds.event_type;
        INSERT INTO tQNEvents VALUES (regid, event_type);
        numqueries := 0;
    
        IF (event_type = DBMS_CQ_NOTIFICATION.EVENT_QUERYCHANGE) THEN
            numqueries := ntfnds.query_desc_array.count;
            FOR i IN 1 .. numqueries LOOP
                qid := ntfnds.query_desc_array(i).queryid;
                qop := ntfnds.query_desc_array(i).queryop;
                INSERT INTO tQNQueries VALUES (qid, qop);
                numtables := 0;
                numtables := ntfnds.query_desc_array(i).table_desc_array.count;
                FOR j IN 1 .. numtables LOOP
                    tbname         := ntfnds.query_desc_array(i).table_desc_array(j).table_name;
                    operation_type := ntfnds.query_desc_array(i).table_desc_array(j).Opflags;
                    INSERT INTO tQNTablechanges VALUES (qid, tbname, operation_type);
                    IF (bitand(operation_type, DBMS_CQ_NOTIFICATION.ALL_ROWS) = 0) THEN
                        numrows := ntfnds.query_desc_array(i).table_desc_array(j).numrows;
                    ELSE
                        numrows := 0; -- ROWID info not available
                    END IF;
                
                    /* Body of loop does not execute when numrows is zero */
                    FOR k IN 1 .. numrows LOOP
                        Row_id := ntfnds.query_desc_array(i).table_desc_array(j).row_desc_array(k).row_id;
                        INSERT INTO tQNRowchanges VALUES (qid, tbname, Row_id);
                    END LOOP; -- loop over rows
                END LOOP; -- loop over tables
            END LOOP; -- loop over queries
        END IF;
    
        COMMIT;
    END prcQNCallback;

END pkgQueryNotifier;
/
