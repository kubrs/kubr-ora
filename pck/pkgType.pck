CREATE OR REPLACE PACKAGE pkgType IS

    -- Author  : PETRESCU Cezar
    -- Purpose : Custom Types Collection

    -- Public type declarations
    TYPE KubrExceptionContainer IS TABLE OF VARCHAR2(500) INDEX BY VARCHAR2(6);

END pkgType;
/
CREATE OR REPLACE PACKAGE BODY pkgType IS
END pkgType;
/
