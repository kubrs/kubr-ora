CREATE OR REPLACE NONEDITIONABLE PACKAGE pkgMisc IS

    -- Author  : PETRESCU Cezar
    -- Purpose : Additional utilitary functions that do not fit in other packages

    FUNCTION repl
    (
        pStr  IN VARCHAR2
       ,pWhat IN VARCHAR2
       ,pWith IN VARCHAR2
    ) RETURN VARCHAR2;
    FUNCTION convertException(pMessage IN VARCHAR2) RETURN VARCHAR2;
    FUNCTION toLongStatus(pStatus IN VARCHAR2) RETURN VARCHAR2;
    FUNCTION toString(pHousekeeping IN thousekeeper%ROWTYPE) RETURN VARCHAR2;
    PROCEDURE writeToConsole(pMessage IN VARCHAR2);

END pkgMisc;
/
CREATE OR REPLACE NONEDITIONABLE PACKAGE BODY pkgMisc IS

    -- Local variables
    gToStringLimit NUMBER := 500;

    -- Replace implementation
    FUNCTION repl
    (
        pStr  IN VARCHAR2
       ,pWhat IN VARCHAR2
       ,pWith IN VARCHAR2
    ) RETURN VARCHAR2 IS
    BEGIN
        RETURN regexp_replace(pStr, pWhat, pWith, 1, 1);
    END repl;

    -- Convert exception
    FUNCTION convertException(pMessage IN VARCHAR2) RETURN VARCHAR2 IS
    BEGIN
        RETURN pkgMisc.repl(pMessage
                           ,pkgConstant.application.oracle || pkgConstant.signs.mi
                           ,pkgConstant.application.exceptionPrefix || pkgConstant.signs.mi);
    END convertException;

    -- Expand status code to long status, if available
    FUNCTION toLongStatus(pStatus IN VARCHAR2) RETURN VARCHAR2 IS
        vres VARCHAR2(10);
    BEGIN
        vres := CASE
                    WHEN pStatus = pkgConstant.application.dbObjectLocked THEN
                     pkgConstant.application.dbObjectLockedLong
                    ELSE
                     pStatus
                END;
        RETURN vres;
    END toLongStatus;

    -- ToString implementation for tHousekeeper record
    FUNCTION toString(pHousekeeping IN thousekeeper%ROWTYPE) RETURN VARCHAR2 IS
        vres tkeyddlatempts.description%TYPE;
    BEGIN
        vres := substr(pHousekeeping.Obj_Type || pkgConstant.signs.dot || pHousekeeping.Obj_Owner ||
                       pkgConstant.signs.dot || pHousekeeping.Obj_Name || ' is in status ' ||
                       toLongStatus(pHousekeeping.Obj_Status) || ', being held by ' || pHousekeeping.Usr_Who || '.'
                      ,1
                      ,gToStringLimit);
        RETURN vres;
    END toString;

    -- Write message to console
    PROCEDURE writeToConsole(pMessage IN VARCHAR2) IS
    BEGIN
        dbms_output.put_line(pMessage);
    END writeToConsole;

END pkgMisc;
/
