CREATE OR REPLACE NONEDITIONABLE PACKAGE pkgException IS

    -- Author  : CEZAR@SHUTTLELITE
    -- Purpose : Define all exceptions used application wide

    -- Exceptions codes and messages
    TYPE typeExceptionList IS RECORD(
         unique_contraint_violated_code PLS_INTEGER := -00001
        ,unique_contraint_violated_text VARCHAR2(100) := 'Unique contraint violated'
        ,check_contraint_violated_code  PLS_INTEGER := -02290
        ,check_contraint_violated_text  VARCHAR2(100) := 'Check contraint violated'
        ,undefined_dbobject_code        PLS_INTEGER := -20010
        ,undefined_dbobject_text        VARCHAR2(100) := 'Undefined database object. Check commited file''s [' ||
                                                        pkgConstant.application.section_config ||
                                                        '] section'
        ,bad_dbobject_code              PLS_INTEGER := -20011
        ,bad_dbobject_text              VARCHAR2(100) := 'Undefined database object [^].'
        -- ----------------------------------------------------------------------------------------------------------                                                        
        ,empty_vcs_signal_code        PLS_INTEGER := -20020
        ,empty_vcs_signal_text        VARCHAR2(100) := 'VCS signal is empty'
        ,empty_vcs_signal_attrib_code PLS_INTEGER := -20030
        ,empty_vcs_signal_attrib_text VARCHAR2(100) := 'Mandatory VCS signal attribute ^ is empty'
        -- ----------------------------------------------------------------------------------------------------------
        ,claw_almighty_code            PLS_INTEGER := -20039
        ,claw_almighty_text            VARCHAR2(100) := 'KUBR is almighty and cannot be touched!'
        ,claw_abusive_release_try_code PLS_INTEGER := -20040
        ,claw_abusive_release_try_text VARCHAR2(100) := 'Abusive release try. Cancelled'
        ,claw_obj_already_locked_code  PLS_INTEGER := -20050
        ,claw_obj_already_locked_text  VARCHAR2(100) := 'Object [^] already locked'
        ,claw_obj_is_locked_code       PLS_INTEGER := -20060
        ,claw_obj_is_locked_text       VARCHAR2(100) := 'Object [^] is locked. No DDL allowed.'
        -- ----------------------------------------------------------------------------------------------------------
        ,io_duplicate_user_code   PLS_INTEGER := -20100
        ,io_duplicate_user_text   VARCHAR2(100) := 'Duplicate user. Cannot create ^'
        ,io_missing_username_code PLS_INTEGER := -20110
        ,io_missing_username_text VARCHAR2(100) := 'Missing "USERNAME" attribute in JSON request'
        -- ----------------------------------------------------------------------------------------------------------
        ,io_registered_object_code     PLS_INTEGER := -20200
        ,io_registered_object_text     VARCHAR2(100) := 'You already registered [^] for monitoring'
        ,io_registered_event_code      PLS_INTEGER := -20201
        ,io_registered_event_text      VARCHAR2(100) := 'Event [^] already registered for monitoring'
        ,io_reg_undefined_event_code   PLS_INTEGER := -20202
        ,io_reg_undefined_event_text   VARCHAR2(100) := 'Event [^] is not defined for monitoring'
        ,io_reg_doubleentry_event_code PLS_INTEGER := -20203
        ,io_reg_doubleentry_event_text VARCHAR2(100) := 'Event [^] does not fit in the current list'
        ,io_notregistered_event_code   PLS_INTEGER := -20204
        ,io_notregistered_event_text   VARCHAR2(100) := 'Event [^] not registered for monitoring'
        -- ----------------------------------------------------------------------------------------------------------        
        );
    exceptionList typeExceptionList;

    -- ORA-00001 error: unique constraint ^ violated 
    unique_contraint_violated EXCEPTION;
    PRAGMA EXCEPTION_INIT(unique_contraint_violated, -00001);
    -- ORA-02290 error: check contraint violated
    check_contraint_violated EXCEPTION;
    PRAGMA EXCEPTION_INIT(check_contraint_violated, -02290);

    -- CLAW-20010 error: undefined database object
    undefined_dbobject EXCEPTION;
    PRAGMA EXCEPTION_INIT(undefined_dbobject, -20010);
    -- CLAW-20011 error: undefined database object
    bad_dbobject EXCEPTION;
    PRAGMA EXCEPTION_INIT(bad_dbobject, -20011);
    -- CLAW-20020 error: VCS signal is empty
    empty_vcs_signal EXCEPTION;
    PRAGMA EXCEPTION_INIT(empty_vcs_signal, -20020);
    -- CLAW-20030 error: Mandatory VCS signal attribute ^ is empty
    empty_vcs_signal_attrib EXCEPTION;
    PRAGMA EXCEPTION_INIT(empty_vcs_signal_attrib, -20030);
    -- CLAW-20039 error: KUBR is almighty and cannot be touched!
    claw_almighty EXCEPTION;
    PRAGMA EXCEPTION_INIT(claw_almighty, -20039);
    -- CLAW-20040 error: Abusive release try. Cancelled
    claw_abusive_release_try EXCEPTION;
    PRAGMA EXCEPTION_INIT(claw_abusive_release_try, -20040);
    -- CLAW-20050 error: Object [^] already locked
    claw_obj_already_locked EXCEPTION;
    PRAGMA EXCEPTION_INIT(claw_obj_already_locked, -20050);
    -- CLAW-20060 error: Object [^] is locked. No DDL allowed.
    claw_obj_is_locked EXCEPTION;
    PRAGMA EXCEPTION_INIT(claw_obj_is_locked, -20060);

    -- CLAW-20100 error: Duplicate user. Cannot create ^
    io_duplicate_user EXCEPTION;
    PRAGMA EXCEPTION_INIT(io_duplicate_user, -20100);
    -- CLAW-20110 error: Missing "USERNAME" attribute in JSON request
    io_missing_username EXCEPTION;
    PRAGMA EXCEPTION_INIT(io_missing_username, -20110);

    -- CLAW-20200 error: You already registered ^ for monitoring
    io_registered_object EXCEPTION;
    PRAGMA EXCEPTION_INIT(io_registered_object, -20200);
    -- CLAW-20201 error: Event [^] already registered for monitoring
    io_registered_event EXCEPTION;
    PRAGMA EXCEPTION_INIT(io_registered_event, -20201);
    -- CLAW-20202 error: Event [^] is not defined for monitoring
    io_reg_undefined_event EXCEPTION;
    PRAGMA EXCEPTION_INIT(io_reg_undefined_event, -20202);
    -- CLAW-20203 error: Event [^] does not fit in the current list
    io_reg_doubleentry_event EXCEPTION;
    PRAGMA EXCEPTION_INIT(io_reg_doubleentry_event, -20203);
    -- CLAW-20204 error:Event [^] not registered for monitoring
    io_notregistered_event EXCEPTION;
    PRAGMA EXCEPTION_INIT(io_notregistered_event, -20204);

    PROCEDURE raiseException
    (
        pExceptionCode       IN NUMBER
       ,pExceptionAddMessage IN VARCHAR2 DEFAULT NULL
    );
    PROCEDURE raiseException
    (
        pexceptioncode       IN NUMBER
       ,pexceptionaddmessage IN VARCHAR2
       ,pexceptionclob       IN CLOB
    );
    PROCEDURE raiseException
    (
        pexceptioncode IN NUMBER
       ,pDbObject      IN typeDbObject
    );

    PROCEDURE logException
    (
        pExceptionCode    IN NUMBER
       ,pExceptionMessage IN VARCHAR2
       ,pwho              IN VARCHAR2 DEFAULT NULL
    );
    PROCEDURE logException
    (
        pexceptioncode    IN NUMBER
       ,pexceptionmessage IN VARCHAR2
       ,pexceptionclob    IN CLOB
    );

END pkgException;
/
CREATE OR REPLACE NONEDITIONABLE PACKAGE BODY pkgException IS

    exceptionsContainer pkgType.KubrExceptionContainer;

    PROCEDURE addException
    (
        pexceptioncode    IN NUMBER
       ,pexceptionmessage IN VARCHAR2
    ) IS
    BEGIN
        exceptionsContainer(pexceptioncode) := pexceptionmessage;
    END addException;

    FUNCTION getMessage(pexceptioncode IN NUMBER) RETURN VARCHAR2 IS
    BEGIN
        RETURN exceptionsContainer(pexceptioncode);
    END getMessage;

    FUNCTION getMessage
    (
        pexceptioncode       IN NUMBER
       ,pexceptionaddmessage IN VARCHAR2
    ) RETURN VARCHAR2 IS
    BEGIN
        RETURN REPLACE(exceptionsContainer(pexceptioncode), pkgConstant.signs.up, pexceptionaddmessage);
    END getMessage;

    PROCEDURE initExceptionMessages IS
    BEGIN
        addexception(pexceptioncode    => exceptionList.check_contraint_violated_code
                    ,pexceptionmessage => exceptionList.check_contraint_violated_text);
        addexception(pexceptioncode    => exceptionList.check_contraint_violated_code
                    ,pexceptionmessage => exceptionList.check_contraint_violated_text);
        addexception(pexceptioncode    => exceptionList.undefined_dbobject_code
                    ,pexceptionmessage => exceptionList.undefined_dbobject_text);
        addexception(pexceptioncode    => exceptionList.bad_dbobject_code
                    ,pexceptionmessage => exceptionList.bad_dbobject_text);
        addexception(pexceptioncode    => exceptionList.empty_vcs_signal_code
                    ,pexceptionmessage => exceptionList.empty_vcs_signal_text);
        addexception(pexceptioncode    => exceptionList.empty_vcs_signal_attrib_code
                    ,pexceptionmessage => exceptionList.empty_vcs_signal_attrib_text);
        addexception(pexceptioncode    => exceptionList.claw_almighty_code
                    ,pexceptionmessage => exceptionList.claw_almighty_text);
        addexception(pexceptioncode    => exceptionList.claw_abusive_release_try_code
                    ,pexceptionmessage => exceptionList.claw_abusive_release_try_text);
        addexception(pexceptioncode    => exceptionList.io_duplicate_user_code
                    ,pexceptionmessage => exceptionList.io_duplicate_user_text);
        addexception(pexceptioncode    => exceptionList.claw_obj_already_locked_code
                    ,pexceptionmessage => exceptionList.claw_obj_already_locked_text);
        addException(pexceptioncode    => exceptionList.io_missing_username_code
                    ,pexceptionmessage => exceptionList.io_missing_username_text);
        addException(pexceptioncode    => exceptionList.io_registered_object_code
                    ,pexceptionmessage => exceptionList.io_registered_object_text);
        addException(pexceptioncode    => exceptionList.io_registered_event_code
                    ,pexceptionmessage => exceptionList.io_registered_event_text);
        addException(pexceptioncode    => exceptionList.io_reg_undefined_event_code
                    ,pexceptionmessage => exceptionList.io_reg_undefined_event_text);
        addException(pexceptioncode    => exceptionList.io_reg_doubleentry_event_code
                    ,pexceptionmessage => exceptionList.io_reg_doubleentry_event_text);
        addException(pexceptioncode    => exceptionList.io_notregistered_event_code
                    ,pexceptionmessage => exceptionList.io_notregistered_event_text);
    END initExceptionMessages;

    PROCEDURE raiseException
    (
        pexceptioncode       IN NUMBER
       ,pexceptionaddmessage IN VARCHAR2 DEFAULT NULL
    ) IS
    BEGIN
        IF (pexceptioncode < -20001) THEN
            logException(pexceptioncode, pkgexception.getmessage(pexceptioncode, pexceptionaddmessage));
            raise_application_error(pexceptioncode, pkgexception.getmessage(pexceptioncode, pexceptionaddmessage));
        ELSE
            logException(pexceptioncode, pexceptionaddmessage);
            raise_application_error(-20001, pexceptionaddmessage);
        END IF;
    END raiseException;

    PROCEDURE raiseException
    (
        pexceptioncode       IN NUMBER
       ,pexceptionaddmessage IN VARCHAR2
       ,pexceptionclob       IN CLOB
    ) IS
    BEGIN
        IF (pexceptioncode < -20001) THEN
            logException(pexceptioncode
                        ,pkgMisc.convertException(pkgexception.getmessage(pexceptioncode, pexceptionaddmessage))
                        ,pexceptionclob);
            raise_application_error(pexceptioncode
                                   ,pkgMisc.convertException(pkgexception.getmessage(pexceptioncode
                                                                                    ,pexceptionaddmessage)));
        END IF;
    END raiseException;

    PROCEDURE raiseException
    (
        pexceptioncode IN NUMBER
       ,pDbObject      IN typeDbObject
    ) IS
    BEGIN
        IF (pexceptioncode < -20001) THEN
            logException(pexceptioncode, pkgexception.getmessage(pexceptioncode, pDbObject.getFullyQualifiedName));
            raise_application_error(pexceptioncode
                                   ,pkgexception.getmessage(pexceptioncode, pDbObject.getFullyQualifiedName));
        END IF;
    END raiseException;

    PROCEDURE logException
    (
        pexceptioncode    IN NUMBER
       ,pexceptionmessage IN VARCHAR2
       ,pwho              IN VARCHAR2 DEFAULT NULL
    ) IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        vmsg VARCHAR2(4000);
    BEGIN
        IF (pexceptioncode < -20001) THEN
            vmsg := pkgConstant.application.exceptionPrefix || pexceptioncode || ': ' || pexceptionmessage;
            ---vmsg := pkgMisc.convertException(pexceptionmessage);
        ELSE
            vmsg := pexceptionmessage;
        END IF;
    
        INSERT INTO tlogexceptions
            (code, message, logwhen, logwho)
        VALUES
            (pexceptioncode, vmsg, systimestamp, nvl(pwho, pkgConstant.application.kubot));
        COMMIT;
    END logException;

    PROCEDURE logException
    (
        pexceptioncode    IN NUMBER
       ,pexceptionmessage IN VARCHAR2
       ,pexceptionclob    IN CLOB
    ) IS
        vwho VARCHAR2(20);
    BEGIN
        BEGIN
            SELECT json_value(pexceptionclob, '$.AUTHOR') INTO vwho FROM dual;
        EXCEPTION
            WHEN OTHERS THEN
                NULL;
        END;
        logException(pexceptioncode, pexceptionmessage, vwho);
    END logException;

BEGIN
    initExceptionMessages;
END pkgException;
/
