CREATE OR REPLACE NONEDITIONABLE PACKAGE pkgConstant IS

    -- Author  : CEZAR@SHUTTLELITE
    -- Purpose : Application scope constants

    -- Application own constants
    TYPE typeApplication IS RECORD(
         SELF               VARCHAR2(4) := 'KUBR'
        ,dbObjectLocked     VARCHAR2(1) := 'L'
        ,dbObjectLockedLong VARCHAR2(10) := 'LOCKED'
        ,kubot              VARCHAR2(10) := 'KUBOT'
        ,section_config     VARCHAR2(10) := 'CONFIG'
        ,section_sql        VARCHAR2(10) := 'SQL'
        ,exceptionPrefix    VARCHAR2(10) := 'KUB'
        ,oracle             VARCHAR2(10) := 'ORA');
    application typeApplication;

    TYPE typeIO IS RECORD(
         username VARCHAR2(20) := '$.USERNAME'
        ,author   VARCHAR2(20) := '$.AUTHOR');
    io typeIO;

    TYPE typeSigns IS RECORD(
         dot VARCHAR2(1) := '.'
        ,eq  VARCHAR2(1) := '='
        ,lf  VARCHAR2(2) := chr(10)
        ,up  VARCHAR2(1) := '^'
        ,mi  VARCHAR2(1) := '-');
    signs typeSigns;

    TYPE typeSignals IS RECORD(
         passed VARCHAR2(10) := 'PASSED'
        ,failed VARCHAR2(10) := 'FAILED');
    signal typeSignals;

    TYPE typeDDLSystemEvents IS RECORD(
         qbrANY      VARCHAR2(10) := '*'
        ,qbrLOCK     VARCHAR2(10) := 'LOCK'
        ,qbrRELEASE  VARCHAR2(10) := 'RELEASE'
        ,sysCREATE   VARCHAR2(10) := 'CREATE'
        ,sysREPLACE  VARCHAR2(10) := 'REPLACE'
        ,sysDROP     VARCHAR2(10) := 'DROP'
        ,sysTRUNCATE VARCHAR2(10) := 'TRUNCATE'
        ,sysALTER    VARCHAR2(10) := 'ALTER');
    DDLSystemEvent typeDDLSystemEvents;

    -- Mandatory [CONFIG] section attributes from SVN DDL files
    configDbObjecttype  CONSTANT VARCHAR2(20) := 'dbobjecttype';
    configDbObjectowner CONSTANT VARCHAR2(20) := 'dbobjectowner';
    configDbObjectname  CONSTANT VARCHAR2(20) := 'dbobjectname';
    -- Invisible [CONFIG] section attributes from SVN DDL files
    configOperation CONSTANT VARCHAR2(20) := 'operation';

END pkgConstant;
/
CREATE OR REPLACE NONEDITIONABLE PACKAGE BODY pkgConstant IS
END pkgConstant;
/
