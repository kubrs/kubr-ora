CREATE OR REPLACE PACKAGE pkgVCS IS

    -- Author  : PETRESCU Cezar
    -- Purpose : Version Control System Connector

    TYPE CommandsLookupType IS TABLE OF VARCHAR2(2) INDEX BY NATURAL;
    SUBTYPE Command IS NATURAL RANGE 1 .. 2 NOT NULL;
    svnCommLookup CommandsLookupType;

    TYPE KubrResponseType IS RECORD(
         code    VARCHAR2(6)
        ,message VARCHAR2(3999));
    kubrResponse KubrResponseType;

    -- VCS Commands | Make sure to define all range of SUBTYPE 'Command'
    setLock     CONSTANT NATURAL := 1; -- 'SL'
    releaseLock CONSTANT NATURAL := 2; -- 'RL'
    -- VCS Signaling return codes
    responseOK  CONSTANT VARCHAR2(2) := 'OK';
    responseNOK CONSTANT VARCHAR2(3) := 'NOK';

    -- FUNCTION signal(pSignal IN VARCHAR2) RETURN KubrResponseType;
    FUNCTION getCommandCode(pCommandNumber IN NATURAL) RETURN VARCHAR2;

END pkgVCS;
/
CREATE OR REPLACE PACKAGE BODY pkgVCS IS

    PROCEDURE initialize IS
    BEGIN
        svnCommLookup(1) := 'SL'; -- corresponds to pkgConstant.svn_setLock
        svnCommLookup(2) := 'RL'; -- corresponds to pkgConstant.svn_releaseLock
    END initialize;

    FUNCTION getCommandCode(pCommandNumber IN NATURAL) RETURN VARCHAR2 IS
    BEGIN
        RETURN svnCommLookup(pCommandNumber);
    END getCommandCode;

    FUNCTION getSignalValue
    (
        psignal IN VARCHAR2
       ,pkey    IN VARCHAR2
    ) RETURN VARCHAR2 IS
        vret VARCHAR2(2000);
    BEGIN
        pkgVCS.kubrResponse.code := responseOK;
        dbms_output.put_line(vret);
        vret := substr(psignal, instr(psignal, pkey, 1, 1), length(psignal));
        dbms_output.put_line(vret);
        vret := substr(vret, instr(vret, pkgConstant.signs.eq, 1, 1) + 1, length(vret));
        dbms_output.put_line(vret);
        vret := substr(vret, 1, instr(vret, pkgConstant.signs.lf, 1, 1) - 1);
    
        IF (vret IS NULL) THEN
            pkgVCS.kubrResponse.code    := responseNOK;
            pkgVCS.kubrResponse.message := pkgVCS.kubrResponse.message || 'Attribute ' || pkey || ' is empty.' ||
                                           pkgConstant.signs.lf;
        END IF;
        dbms_output.put_line(pkey || ':' || vret);
        dbms_output.put_line('---------------------------------');
        RETURN vret;
    END getSignalValue;

    -- Validate incomming VCS signal
    FUNCTION isSignalValid
    (
        pSignal   IN VARCHAR2
       ,pDbObject IN OUT typeDbObject
    ) RETURN BOOLEAN IS
        vdbobjtype  VARCHAR2(30);
        vdbobjowner VARCHAR2(30);
        vdbobjname  VARCHAR2(30);
        -- voperation  VARCHAR2(2);
    BEGIN
    
        IF (pSignal IS NULL) THEN
            pkgVCS.kubrResponse.code    := responseNOK;
            pkgVCS.kubrResponse.message := pkgException.exceptionList.empty_vcs_signal_text;
            RAISE pkgException.empty_vcs_signal;
        END IF;
    
        pkgVCS.kubrResponse.message := pkgConstant.signs.lf;
        vdbobjtype                  := pkgVCS.getSignalValue(psignal, pkgconstant.configDbObjectType);
        vdbobjowner                 := pkgVCS.getSignalValue(psignal, pkgconstant.configdbobjectowner);
        vdbobjname                  := pkgVCS.getSignalValue(psignal, pkgconstant.configdbobjectname);
        -- voperation                  := pkgVCS.getSignalValue(psignal, pkgconstant.configoperation);
    
        IF (pkgVCS.kubrResponse.code = responseNOK) THEN
            RAISE pkgException.empty_vcs_signal_attrib;
        END IF;
    
        pDbObject := NEW typedbobject(vdbobjtype, vdbobjowner, vdbobjname);
        RETURN(pkgVCS.kubrResponse.code = responseOK);
    EXCEPTION
        WHEN pkgException.empty_vcs_signal THEN
            RETURN(FALSE);
        WHEN pkgException.empty_vcs_signal_attrib THEN
            RETURN(FALSE);
    END isSignalValid;

-- Process VCS Signal
/*    FUNCTION signal(pSignal IN VARCHAR2) RETURN KubrResponseType IS
        vDbObject     typeDbObject;
        vSignalResult BOOLEAN;
    BEGIN
        IF (isSignalValid(pSignal, vDbObject)) THEN
            vSignalResult := pkgClaw.lockDbObject(vDbObject);
        END IF;
    
        pkgException.logException(-1, pSignal);
        RETURN(kubrResponse);
    END signal;*/

BEGIN
    initialize;
END pkgVCS;
/
