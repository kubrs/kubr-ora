CREATE OR REPLACE PACKAGE kubrIO IS

    -- Author  : CEZAR@SHUTTLELITE
    -- Purpose : I/O wrappers

    SUBTYPE kioUser IS ioUser;

    PROCEDURE lockDbObject
    (
        pRepoPath   IN VARCHAR2
       ,pFilePath   IN VARCHAR2
       ,pRepoUser   IN VARCHAR2
       ,pRepoComm   IN VARCHAR2
       ,pSteal      IN VARCHAR2
       ,pdbObjOwner IN VARCHAR2
       ,pdbObjType  IN VARCHAR2
       ,pdbObjName  IN VARCHAR2
    );

    PROCEDURE releaseDbObject
    (
        pRepoPath   IN VARCHAR2
       ,pFilePath   IN VARCHAR2
       ,pRepoUser   IN VARCHAR2
       ,pdbObjOwner IN VARCHAR2
       ,pdbObjType  IN VARCHAR2
       ,pdbObjName  IN VARCHAR2
    );

    PROCEDURE registerInterest
    (
        pType           IN thousekeeper.obj_type%TYPE
       ,pOwner          IN thousekeeper.obj_owner%TYPE
       ,pName           IN thousekeeper.obj_name%TYPE
       ,pEventToMonitor IN tregistration_targets.listening_on%TYPE DEFAULT NULL
    );

    PROCEDURE unregisterInterest
    (
        pType           IN thousekeeper.obj_type%TYPE
       ,pOwner          IN thousekeeper.obj_owner%TYPE
       ,pName           IN thousekeeper.obj_name%TYPE
       ,pEventToMonitor IN tregistration_targets.listening_on%TYPE DEFAULT NULL
    );

END kubrIO;
/
CREATE OR REPLACE PACKAGE BODY kubrIO IS

    -- Describe current execution of a given routine
    PROCEDURE describeRoutine
    (
        pRoutine    IN VARCHAR2
       ,pdbObjOwner IN VARCHAR2
       ,pdbObjType  IN VARCHAR2
       ,pdbObjName  IN VARCHAR2
    ) IS
    BEGIN
        pkgMisc.writeToConsole(pkgConstant.application.SELF || pkgConstant.signs.dot || pRoutine || '()');
        pkgMisc.writeToConsole('Parameters:');
        pkgMisc.writeToConsole('  Type   = ' || pdbObjType);
        pkgMisc.writeToConsole('  Owner  = ' || pdbObjOwner);
        pkgMisc.writeToConsole('  Object = ' || pdbObjName);
    END describeRoutine;

    -- Describe current execution of a given routine
    PROCEDURE describeResult(pResult IN BOOLEAN) IS
    BEGIN
        IF pResult THEN
            pkgMisc.writeToConsole('Execution SUCCESSFUL.');
        ELSE
            pkgMisc.writeToConsole('Execution FAILED.');
        END IF;
    END describeResult;

    -- Wrapper for Set Lock on database object based on given typeDbObject
    PROCEDURE lockDbObject
    (
        pRepoPath   IN VARCHAR2
       ,pFilePath   IN VARCHAR2
       ,pRepoUser   IN VARCHAR2
       ,pRepoComm   IN VARCHAR2
       ,pSteal      IN VARCHAR2
       ,pdbObjOwner IN VARCHAR2
       ,pdbObjType  IN VARCHAR2
       ,pdbObjName  IN VARCHAR2
    ) IS
    BEGIN
        describeRoutine('lockDbObject', pdbObjOwner, pdbObjType, pdbObjName);
        describeResult(pkgclaw.lockDbObject(pRepoPath   => pRepoPath
                                           ,pFilePath   => pFilePath
                                           ,pRepoUser   => pRepoUser
                                           ,pRepoComm   => pRepoComm
                                           ,pSteal      => pSteal
                                           ,pdbobjowner => pdbObjOwner
                                           ,pdbobjtype  => pdbObjType
                                           ,pdbobjname  => pdbObjName));
    END lockDbObject;

    -- Wrapper for Release Lock on database object based on given TypeDbObject
    PROCEDURE releaseDbObject
    (
        pRepoPath   IN VARCHAR2
       ,pFilePath   IN VARCHAR2
       ,pRepoUser   IN VARCHAR2
       ,pdbObjOwner IN VARCHAR2
       ,pdbObjType  IN VARCHAR2
       ,pdbObjName  IN VARCHAR2
    ) IS
    BEGIN
        describeRoutine('releaseDbObject', pdbObjOwner, pdbObjType, pdbObjName);
        describeResult(pkgClaw.releaseDbObject(pRepoPath   => pRepoPath
                                              ,pFilePath   => pFilePath
                                              ,pRepoUser   => pRepoUser
                                              ,pdbobjowner => pdbObjOwner
                                              ,pdbobjtype  => pdbObjType
                                              ,pdbobjname  => pdbObjName));
    END releaseDbObject;

    -- Wrapper for Registering interest for db object
    PROCEDURE registerInterest
    (
        pType           IN thousekeeper.obj_type%TYPE
       ,pOwner          IN thousekeeper.obj_owner%TYPE
       ,pName           IN thousekeeper.obj_name%TYPE
       ,pEventToMonitor IN tregistration_targets.listening_on%TYPE DEFAULT NULL
    ) IS
    BEGIN
        pkgRegistrations.registerInterest(typedbobject(upper(pOwner), upper(pType), upper(pName)), pEventToMonitor);
    END registerInterest;

    -- Wrapper for Unregistering interest for db object
    PROCEDURE unregisterInterest
    (
        pType           IN thousekeeper.obj_type%TYPE
       ,pOwner          IN thousekeeper.obj_owner%TYPE
       ,pName           IN thousekeeper.obj_name%TYPE
       ,pEventToMonitor IN tregistration_targets.listening_on%TYPE DEFAULT NULL
    ) IS
    BEGIN
        pkgRegistrations.unregisterInterest(typedbobject(upper(pOwner), upper(pType), upper(pName)), pEventToMonitor);
    END unregisterInterest;

--

END kubrIO;
/
