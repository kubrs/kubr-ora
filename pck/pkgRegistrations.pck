CREATE OR REPLACE PACKAGE pkgRegistrations IS

    -- Author  : CEZAR@SHUTTLELITE
    -- Created : 11/22/2017
    -- Purpose : Registrations Manager

    PROCEDURE registerInterest
    (
        pDbObject       IN typedbobject
       ,pEventToMonitor IN tregistration_targets.listening_on%TYPE DEFAULT NULL
    );
    PROCEDURE unregisterInterest
    (
        pDbObject       IN typedbobject
       ,pEventToMonitor IN tregistration_targets.listening_on%TYPE DEFAULT NULL
    );

END pkgRegistrations;
/
CREATE OR REPLACE PACKAGE BODY pkgRegistrations IS

    FUNCTION existsRegistration
    (
        pDbObject       IN typedbobject
       ,pRegistrationID OUT tRegistrations.Id%TYPE
    ) RETURN BOOLEAN IS
        vres BOOLEAN := FALSE;
        CURSOR cRegistration IS
            SELECT t.id
              FROM tRegistrations t
             WHERE t.ora_login_user = ora_login_user
               AND t.sc_os_user = sc_os_user
               AND t.ora_dict_obj_type = pDbObject.getType
               AND t.ora_dict_obj_owner = pDbObject.getOwner
               AND t.ora_dict_obj_name = pDbObject.getName;
    BEGIN
        FOR rec IN cRegistration LOOP
            pRegistrationID := rec.id;
            vres            := TRUE;
        END LOOP;
        RETURN vres;
    END existsRegistration;

    FUNCTION existsRegistrationTarget
    (
        pDbObject       IN typedbobject
       ,pEventToMonitor IN tregistration_targets.listening_on%TYPE DEFAULT NULL
    ) RETURN BOOLEAN IS
        vres BOOLEAN := FALSE;
        CURSOR cRegistration IS
            SELECT tt.reg_id
                  ,tt.listening_on
              FROM tRegistrations t
             INNER JOIN tRegistration_targets tt
                ON tt.reg_id = t.id
             WHERE t.ora_login_user = ora_login_user
               AND t.sc_os_user = sc_os_user
               AND t.ora_dict_obj_type = pDbObject.getType
               AND t.ora_dict_obj_owner = pDbObject.getOwner
               AND t.ora_dict_obj_name = pDbObject.getName
               AND ((pEventToMonitor IS NULL) OR ((pEventToMonitor IS NOT NULL) AND tt.listening_on = pEventToMonitor));
    BEGIN
        FOR rec IN cRegistration LOOP
            vres := TRUE;
            EXIT;
        END LOOP;
        RETURN vres;
    END existsRegistrationTarget;

    -- register interest for db object
    PROCEDURE registerInterest
    (
        pDbObject       IN typedbobject
       ,pEventToMonitor IN tregistration_targets.listening_on%TYPE DEFAULT NULL
    ) IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        exUKRegObject   EXCEPTION;
        exUKRegEvent    EXCEPTION;
        exCHKRegEvent   EXCEPTION;
        exUKDoubleEvent EXCEPTION;
    
        vIdPk                  tregistrations.id%TYPE;
        sc_os_user             VARCHAR2(30);
        sc_client_program_name VARCHAR2(200);
    BEGIN
        IF NOT pkgClaw.existsDbObject(pDbObject) THEN
            RAISE pkgException.bad_dbobject;
        END IF;
    
        SELECT sys_context('USERENV', 'OS_USER') AS sc_os_user
              ,sys_context('USERENV', 'CLIENT_PROGRAM_NAME') AS sc_client_program_name
          INTO sc_os_user
              ,sc_client_program_name
          FROM dual;
    
        IF NOT existsRegistration(pDbObject, vIdPk) THEN
            vIdPk := seq_tregistrations.nextval;
            BEGIN
                INSERT INTO tRegistrations
                    (id
                    ,Ora_Systime
                    ,Ora_Login_User
                    ,Ora_Dict_Obj_Type
                    ,Ora_Dict_Obj_Owner
                    ,Ora_Dict_Obj_Name
                    ,Sc_Os_User
                    ,Sc_Client_Program_Name)
                VALUES
                    (vIdPk
                    ,systimestamp
                    ,ora_login_user
                    ,pDbObject.getType
                    ,pDbObject.getOwner
                    ,pDbObject.getName
                    ,sc_os_user
                    ,sc_client_program_name);
            EXCEPTION
                WHEN pkgException.unique_contraint_violated THEN
                    RAISE exUKRegObject;
            END;
        END IF;
    
        IF existsRegistrationTarget(pDbObject, Pkgconstant.DDLSystemEvent.qbrANY) THEN
            RAISE exUKDoubleEvent;
        ELSIF ((pEventToMonitor = Pkgconstant.DDLSystemEvent.qbrANY) AND existsRegistrationTarget(pDbObject)) THEN
            RAISE exUKDoubleEvent;
        ELSE
            BEGIN
                INSERT INTO tRegistration_targets
                    (Reg_Id, Listening_On)
                VALUES
                    (vIdPk, nvl(pEventToMonitor, Pkgconstant.DDLSystemEvent.qbrANY));
            EXCEPTION
                WHEN pkgException.check_contraint_violated THEN
                    RAISE exCHKRegEvent;
                WHEN pkgException.unique_contraint_violated THEN
                    RAISE exUKRegEvent;
            END;
        END IF;
    
        COMMIT;
    EXCEPTION
        WHEN exUKDoubleEvent THEN
            pkgException.raiseException(pkgException.exceptionList.io_reg_doubleentry_event_code
                                       ,nvl(pEventToMonitor, Pkgconstant.signs.mi));
        WHEN exUKRegObject THEN
            pkgException.raiseException(pkgException.exceptionList.io_registered_object_code, pDbObject);
        WHEN exUKRegEvent THEN
            pkgException.raiseException(pkgException.exceptionList.io_registered_event_code
                                       ,nvl(pEventToMonitor, Pkgconstant.DDLSystemEvent.qbrANY));
        WHEN exCHKRegEvent THEN
            pkgException.raiseException(pkgException.exceptionList.io_reg_undefined_event_code
                                       ,nvl(pEventToMonitor, Pkgconstant.DDLSystemEvent.qbrANY));
        WHEN pkgException.bad_dbobject THEN
            pkgException.raiseException(pkgException.exceptionList.bad_dbobject_code, pDbObject);
    END registerInterest;

    -- unregister interest for db object
    PROCEDURE unregisterInterest
    (
        pDbObject       IN typedbobject
       ,pEventToMonitor IN tregistration_targets.listening_on%TYPE DEFAULT NULL
    ) IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        exNotRegisteredEvent EXCEPTION;
        sc_os_user             VARCHAR2(30);
        sc_client_program_name VARCHAR2(200);
    BEGIN
        IF NOT pkgClaw.existsDbObject(pDbObject) THEN
            RAISE pkgException.bad_dbobject;
        END IF;
    
        IF ((pEventToMonitor IS NOT NULL) AND (NOT existsRegistrationTarget(pDbObject, pEventToMonitor))) THEN
            RAISE exNotRegisteredEvent;
        END IF;
    
        SELECT sys_context('USERENV', 'OS_USER') AS sc_os_user
              ,sys_context('USERENV', 'CLIENT_PROGRAM_NAME') AS sc_client_program_name
          INTO sc_os_user
              ,sc_client_program_name
          FROM dual;
    
        DELETE FROM tRegistration_targets tt
         WHERE tt.reg_id = (SELECT t.id
                              FROM tRegistrations t
                             WHERE t.ora_login_user = ora_login_user
                               AND t.sc_os_user = sc_os_user
                               AND t.ora_dict_obj_type = pDbObject.getType
                               AND t.ora_dict_obj_owner = pDbObject.getOwner
                               AND t.ora_dict_obj_name = pDbObject.getName)
           AND ((pEventToMonitor IS NULL) OR ((pEventToMonitor IS NOT NULL) AND (tt.listening_on = pEventToMonitor)));
    
        DELETE FROM tRegistrations t
         WHERE t.ora_login_user = ora_login_user
           AND t.sc_os_user = sc_os_user
           AND t.ora_dict_obj_type = pDbObject.getType
           AND t.ora_dict_obj_owner = pDbObject.getOwner
           AND t.ora_dict_obj_name = pDbObject.getName
           AND NOT EXISTS (SELECT tt.reg_id FROM tRegistration_targets tt WHERE tt.reg_id = t.id);
        COMMIT;
    EXCEPTION
        WHEN exNotRegisteredEvent THEN
            pkgException.raiseException(pkgException.exceptionList.io_notregistered_event_code, pEventToMonitor);
        WHEN pkgException.bad_dbobject THEN
            pkgException.raiseException(pkgException.exceptionList.bad_dbobject_code, pDbObject);
    END unregisterInterest;

END pkgRegistrations;
/
