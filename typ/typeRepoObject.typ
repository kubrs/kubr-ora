CREATE OR REPLACE TYPE TypeRepoObject AS OBJECT
(
-- Attributes
    repoPath VARCHAR2(300),
    filePath VARCHAR2(300),
    repoUser VARCHAR2(30),
    repoComm VARCHAR2(2000),
    steal    VARCHAR2(10),

-- Constructors
    CONSTRUCTOR FUNCTION TypeRepoObject(x TypeRepoObject) RETURN SELF AS RESULT,
    CONSTRUCTOR FUNCTION TypeRepoObject
    (
        repoPath VARCHAR2
       ,filePath VARCHAR2
       ,repoUser VARCHAR2
       ,repoComm VARCHAR2
       ,steal    VARCHAR2
    ) RETURN SELF AS RESULT

)
/
CREATE OR REPLACE TYPE BODY TypeRepoObject AS

    -- Constructors
    CONSTRUCTOR FUNCTION TypeRepoObject
    (
        repoPath VARCHAR2
       ,filePath VARCHAR2
       ,repoUser VARCHAR2
       ,repoComm VARCHAR2
       ,steal    VARCHAR2
    ) RETURN SELF AS RESULT IS
    BEGIN
        self.repoPath := repoPath;
        self.filePath := filePath;
        self.repoUser := repoUser;
        self.repoComm := repoComm;
        self.steal    := steal;
        RETURN;
    END TypeRepoObject;

    CONSTRUCTOR FUNCTION TypeRepoObject(x TypeRepoObject) RETURN SELF AS RESULT IS
    BEGIN
        self.repoPath := x.repoPath;
        self.filePath := x.filePath;
        self.repoUser := x.repoUser;
        self.repoComm := x.repoComm;
        self.steal    := x.steal;
        RETURN;
    END TypeRepoObject;
END;
/
