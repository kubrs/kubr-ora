CREATE OR REPLACE TYPE ioUser AS OBJECT
(
    BODY CLOB, -- as JSON

    STATIC FUNCTION whois(x CLOB) RETURN VARCHAR2,
    STATIC FUNCTION alive(x CLOB) RETURN BOOLEAN,
    STATIC FUNCTION hasUsername(x CLOB) RETURN BOOLEAN,

    STATIC PROCEDURE spawn(x CLOB),
    STATIC PROCEDURE redo(x CLOB),
    STATIC PROCEDURE kill(x CLOB)
)
/
CREATE OR REPLACE TYPE BODY ioUser IS

    STATIC FUNCTION hasUsername(x CLOB) RETURN BOOLEAN IS
        vwho VARCHAR2(20);
    BEGIN
        SELECT json_value(x, '$.USERNAME') INTO vwho FROM dual;
        RETURN TRUE;
    EXCEPTION
        WHEN no_data_found THEN
            RETURN FALSE;
    END hasUsername;

    STATIC FUNCTION whois(x CLOB) RETURN VARCHAR2 IS
        vwho VARCHAR2(20);
    BEGIN
        SELECT json_value(x, '$.USERNAME') INTO vwho FROM dual;
        IF (vwho IS NULL) THEN
            pkgException.raiseException(pkgException.exceptionList.io_missing_username_code, pkgException.exceptionList.io_missing_username_text, x);
        ELSE
            RETURN vwho;
        END IF;
    EXCEPTION
    
        WHEN OTHERS THEN
            pkgException.raiseException(SQLCODE, SQLERRM);
    END whois;

    STATIC FUNCTION alive(x CLOB) RETURN BOOLEAN IS
        vwho VARCHAR2(20);
    BEGIN
        vwho := whois(x);
        IF (vwho IS NOT NULL) THEN
            FOR rec IN (SELECT t.id FROM tkusers t WHERE json_value(t.body, '$.USERNAME') = vwho)
            LOOP
                RETURN TRUE;
            END LOOP;
        END IF;
        RETURN FALSE;
    EXCEPTION
        WHEN OTHERS THEN
            pkgException.raiseException(SQLCODE, SQLERRM);
    END alive;

    STATIC PROCEDURE spawn(x CLOB) IS
    BEGIN
        IF (NOT alive(x)) THEN
            INSERT INTO tkusers (BODY) VALUES (x);
        ELSE
            RAISE pkgException.io_duplicate_user;
        END IF;
    EXCEPTION
        WHEN pkgException.io_duplicate_user THEN
            pkgException.raiseException(pExceptionCode => pkgException.exceptionList.io_duplicate_user_code, pExceptionAddMessage => whois(x),
                                        pexceptionclob => x);
        WHEN OTHERS THEN
            pkgException.raiseException(SQLCODE, SQLERRM);
    END spawn;

    STATIC PROCEDURE redo(x CLOB) IS
        vwho VARCHAR2(20);
    BEGIN
        vwho := whois(x);
        UPDATE tkusers t SET t.body = x WHERE json_value(t.body, '$.USERNAME') = vwho;
    EXCEPTION
        WHEN OTHERS THEN
            pkgException.raiseException(SQLCODE, SQLERRM);
    END redo;

    STATIC PROCEDURE kill(x CLOB) IS
        vwho VARCHAR2(20);
    BEGIN
        vwho := whois(x);
        DELETE FROM tkusers t WHERE json_value(t.body, '$.USERNAME') = vwho;
    EXCEPTION
        WHEN OTHERS THEN
            pkgException.raiseException(SQLCODE, SQLERRM);
    END kill;

END;
/
