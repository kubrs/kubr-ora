CREATE OR REPLACE NONEDITIONABLE TYPE TypeDbObject AS OBJECT
(
-- Attributes
    dbObjType    VARCHAR2(30),
    dbObjOwner   VARCHAR2(30),
    dbObjName    VARCHAR2(30),
    dbObjCommand VARCHAR2(2),
    userWho VARCHAR2(30),

-- Constructors
    CONSTRUCTOR FUNCTION TypeDbObject(x TypeDbObject) RETURN SELF AS RESULT,
    CONSTRUCTOR FUNCTION TypeDbObject
    (
        dbObjOwner VARCHAR2
       ,dbObjType  VARCHAR2
       ,dbObjName  VARCHAR2
    ) RETURN SELF AS RESULT,

-- Setters
    MEMBER PROCEDURE setType(x VARCHAR2),
    MEMBER PROCEDURE setOwner(x VARCHAR2),
    MEMBER PROCEDURE setName(x VARCHAR2),
    MEMBER PROCEDURE setCommand(x VARCHAR2),

-- Getters
    MEMBER FUNCTION getType RETURN VARCHAR2,
    MEMBER FUNCTION getOwner RETURN VARCHAR2,
    MEMBER FUNCTION getName RETURN VARCHAR2,
    MEMBER FUNCTION getCommand RETURN VARCHAR2,
    MEMBER FUNCTION getQualifiedName RETURN VARCHAR2,
    MEMBER FUNCTION getFullyQualifiedName RETURN VARCHAR2

)
/
CREATE OR REPLACE NONEDITIONABLE TYPE BODY TypeDbObject AS

    -- Constructors
    CONSTRUCTOR FUNCTION TypeDbObject
    (
        dbObjOwner VARCHAR2
       ,dbObjType  VARCHAR2
       ,dbObjName  VARCHAR2
    ) RETURN SELF AS RESULT IS
    BEGIN
        self.dbobjtype  := dbObjType;
        self.dbobjowner := dbObjOwner;
        self.dbobjname  := dbObjName;
        RETURN;
    END TypeDbObject;

    CONSTRUCTOR FUNCTION TypeDbObject(x TypeDbObject) RETURN SELF AS RESULT IS
    BEGIN
        self.dbobjtype  := x.dbobjtype;
        self.dbobjowner := x.dbobjowner;
        self.dbobjname  := x.dbobjname;
        RETURN;
    END TypeDbObject;

    -- Setters
    MEMBER PROCEDURE setType(x VARCHAR2) IS
    BEGIN
        self.dbobjtype := x;
    END setType;

    MEMBER PROCEDURE setOwner(x VARCHAR2) IS
    BEGIN
        self.dbobjowner := x;
    END setOwner;

    MEMBER PROCEDURE setName(x VARCHAR2) IS
    BEGIN
        self.dbobjname := x;
    END setName;

    MEMBER PROCEDURE setCommand(x VARCHAR2) IS
    BEGIN
        self.dbobjcommand := x;
    END setCommand;

    -- Getters
    MEMBER FUNCTION getType RETURN VARCHAR2 IS
    BEGIN
        RETURN self.dbobjtype;
    END getType;

    MEMBER FUNCTION getOwner RETURN VARCHAR2 IS
    BEGIN
        RETURN self.dbobjowner;
    END getOwner;

    MEMBER FUNCTION getName RETURN VARCHAR2 IS
    BEGIN
        RETURN self.dbobjname;
    END getName;

    MEMBER FUNCTION getCommand RETURN VARCHAR2 IS
    BEGIN
        RETURN self.dbObjCommand;
    END getCommand;

    MEMBER FUNCTION getQualifiedName RETURN VARCHAR2 IS
    BEGIN
        RETURN self.dbobjowner || pkgconstant.signs.dot || self.dbobjname;
    END getQualifiedName;

    MEMBER FUNCTION getFullyQualifiedName RETURN VARCHAR2 IS
    BEGIN
        RETURN self.dbobjtype || pkgconstant.signs.dot || self.getQualifiedName;
    END getFullyQualifiedName;

END;
/
