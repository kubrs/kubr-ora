-- Create table
create table THOUSEKEEPER
(
  obj_id     NUMBER generated always as identity,
  obj_owner  VARCHAR2(30) not null,
  obj_type   VARCHAR2(30) not null,
  obj_name   VARCHAR2(30) not null,
  obj_status VARCHAR2(1) not null,
  usr_who    VARCHAR2(30),
  obj_repo   TYPEREPOOBJECT
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 1M
    next 512K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table THOUSEKEEPER
  is 'Keeps track of locked objects';
-- Add comments to the columns 
comment on column THOUSEKEEPER.obj_id
  is 'Object ID, PK';
comment on column THOUSEKEEPER.obj_owner
  is 'Schema owner of current object';
comment on column THOUSEKEEPER.obj_type
  is 'Object type';
comment on column THOUSEKEEPER.obj_name
  is 'Object name';
comment on column THOUSEKEEPER.obj_status
  is 'Object status';
comment on column THOUSEKEEPER.obj_repo
  is 'Repository details (REPOS, PATH, USER, COMMENT, STEAL)';
-- Create/Recreate primary, unique and foreign key constraints 
alter table THOUSEKEEPER
  add constraint UK_HOUSEKEEP_OWNERNAME unique (OBJ_OWNER, OBJ_NAME)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate check constraints 
alter table THOUSEKEEPER
  add constraint CHK_HOUSEK_STATUS
  check (OBJ_STATUS in ('L', 'F'));
