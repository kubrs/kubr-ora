-- Record notification events.
CREATE TABLE TQNevents (
  regid      NUMBER,
  event_type NUMBER   );

-- Record notification queries.
CREATE TABLE TQNqueries (
  qid NUMBER,
  qop NUMBER           );

-- Record changes to registered tables.
CREATE TABLE TQNtablechanges (
  qid             NUMBER,
  table_name      VARCHAR2(100),
  table_operation NUMBER    );

-- Record ROWIDs of changed rows.
CREATE TABLE TQNrowchanges (
  qid        NUMBER,
  table_name VARCHAR2(100),
  row_id     VARCHAR2(2000));
