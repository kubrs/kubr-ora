create table TABLE1
(
  ts       TIMESTAMP(6) WITH LOCAL TIME ZONE,
  repos    VARCHAR2(255),
  rev      NUMBER,
  txn_name VARCHAR2(255),
  config   VARCHAR2(2000)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on column TABLE1.repos
  is 'the path to this repository';
comment on column TABLE1.rev
  is 'the number of the revision just committed';
comment on column TABLE1.txn_name
  is 'the name of the transaction that has become REV';
comment on column TABLE1.config
  is 'Configuration info as found in the [CONFIG] section of the committed file';

