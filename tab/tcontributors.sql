-- Create table
create table TCONTRIBUTORS
(
  id                  NUMBER generated by default on null as identity,
  objecttype          VARCHAR2(30) not null,
  objectqualifiedname VARCHAR2(66) not null,
  command             VARCHAR2(2) not null,
  changedwhen         TIMESTAMP(6),
  changedwho          VARCHAR2(30),
  obj_repo            TYPEREPOOBJECT
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 2M
    next 512K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table TCONTRIBUTORS
  is 'Central repo of the entire lock/unlock process';
-- Add comments to the columns 
comment on column TCONTRIBUTORS.id
  is 'PK';
comment on column TCONTRIBUTORS.objecttype
  is 'Database object type (see allowed values)';
comment on column TCONTRIBUTORS.objectqualifiedname
  is 'Database object owner.name';
comment on column TCONTRIBUTORS.command
  is 'VCS command to the object (see allowed values)';
comment on column TCONTRIBUTORS.changedwhen
  is 'When the state was last changed';
comment on column TCONTRIBUTORS.changedwho
  is 'Who changed the state';
comment on column TCONTRIBUTORS.obj_repo
  is 'Repository details (REPOS, PATH, USER, COMMENT, STEAL)';
-- Create/Recreate check constraints 
alter table TCONTRIBUTORS
  add constraint CHK_CONTRIB_COMMAND
  check (COMMAND in ('SL', 'RL'));
