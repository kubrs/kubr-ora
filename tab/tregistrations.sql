-- Create table
create table TREGISTRATIONS
(
  id                     NUMBER,
  ora_systime            TIMESTAMP(6),
  ora_login_user         VARCHAR2(30),
  ora_dict_obj_type      VARCHAR2(20),
  ora_dict_obj_owner     VARCHAR2(30),
  ora_dict_obj_name      VARCHAR2(30),
  sc_os_user             VARCHAR2(100),
  sc_client_program_name VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 2M
    next 512K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table TREGISTRATIONS
  is 'Registrations for future notifications on released objects from locks';
-- Add comments to the columns 
comment on column TREGISTRATIONS.id
  is 'PK';
comment on column TREGISTRATIONS.ora_systime
  is 'Event timestamp';
comment on column TREGISTRATIONS.ora_login_user
  is 'Login user name.';
comment on column TREGISTRATIONS.ora_dict_obj_type
  is 'Type of the dictionary object.';
comment on column TREGISTRATIONS.ora_dict_obj_owner
  is 'Owner of the dictionary object.';
comment on column TREGISTRATIONS.ora_dict_obj_name
  is 'Name of the dictionary object.';
comment on column TREGISTRATIONS.sc_os_user
  is 'Operating system user name of the client process that initiated the database session.';
comment on column TREGISTRATIONS.sc_client_program_name
  is 'The name of the program used for the database session.';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TREGISTRATIONS
  add constraint TREG_PK primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table TREGISTRATIONS
  add constraint TREGS_UK unique (SC_OS_USER, ORA_LOGIN_USER, ORA_DICT_OBJ_OWNER, ORA_DICT_OBJ_NAME)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
