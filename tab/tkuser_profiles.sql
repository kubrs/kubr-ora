-- Create table
create table TKUSER_PROFILES
(
  id         NUMBER generated by default on null as identity,
  user_id    NUMBER not null,
  profile_id NUMBER not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the table 
comment on table TKUSER_PROFILES
  is 'Available user profiles';
-- Add comments to the columns 
comment on column TKUSER_PROFILES.user_id
  is 'User FK';
comment on column TKUSER_PROFILES.profile_id
  is 'Profile FK';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TKUSER_PROFILES
  add constraint KUSRPRFL_ID_PK primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table TKUSER_PROFILES
  add constraint KUSRPRFL_PID_FK foreign key (PROFILE_ID)
  references TKPROFILES (ID)
  deferrable initially deferred;
alter table TKUSER_PROFILES
  add constraint KUSRPRFL_UID_FK foreign key (USER_ID)
  references TKUSERS (ID)
  deferrable initially deferred;
