-- Create table
create table TLOGEXCEPTIONS
(
  id      NUMBER generated always as identity,
  code    VARCHAR2(6),
  message VARCHAR2(2000),
  logwhen TIMESTAMP(6),
  logwho  VARCHAR2(30)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 2M
    next 512K
    minextents 1
    maxextents unlimited
  );
/
-- Add comments to the table 
comment on table TLOGEXCEPTIONS
  is 'Log of all exceptions within application scope';
/
